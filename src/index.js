import React from 'react'
import ReactDOM from 'react-dom'
import SearchView from './SearchView'
import './index.scss'

ReactDOM.render(<SearchView />, document.getElementById('root'))
