import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import App from './ResultView'
import {geolocated} from 'react-geolocated';
import TextField from '@material-ui/core/TextField';
import './weather-icons.min.css';
import Search from '@material-ui/icons/Search';


class SearchView extends Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.getLocation = this.getLocation.bind(this);
    }

    handleClick = () => {
        if (!document.getElementById('searchTxt').value) {
            alert('Please enter the location!')
        } else {
            const city = document.getElementById('searchTxt').value;
            localStorage.setItem("cityKey", city);
            const measure = "metric";
            localStorage.setItem("measureKey", measure);
            ReactDOM.render(<App/>, document.getElementById('root'))
        }
    }

    getLocation = () => {
        if (!this.props.isGeolocationAvailable) {
            console.log("Your browser does not support Geolocation");
        }
        if (!this.props.isGeolocationEnabled) {
            console.log("Geolocation is not enabled");
        } else {
            const latitude = this.props.coords.latitude;
            const longitude = this.props.coords.longitude;
            localStorage.setItem("latKey", latitude);
            localStorage.setItem("lonKey", longitude);
            const measure = "metric";
            localStorage.setItem("measureKey", measure);
            ReactDOM.render(<App/>, document.getElementById('root'))
        }
    }

    render() {
        return (
            <div className="search-grid-container">
                <div id="search-middle-div">
                    <div id="aligned-search-items">
                    <div className="search-grid-item search-item1">
                        <TextField name="searchTxt" id="searchTxt" placeholder="Search for location..."/>
                        <Search input id="search-button" type="submit" onClick={this.handleClick} value="Search"/>
                    </div>
                    </div>
                    <div className="search-grid-item search-item2">
                        <input type="link" id="location-link" onClick={this.getLocation} value="Use current location"/>
                    </div>
                </div>
            </div>
        );
    }
}

export default geolocated({
    userDecisionTimeout: 5000,
})(SearchView);