import React, {Component} from 'react';
import ReactDOM from 'react-dom'
import SearchView from './SearchView'
import ArrowBack from '@material-ui/icons/ArrowBack';
import Switch from '@material-ui/core/Switch';

class ResultView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            isLoading: false,
            error: null,
            isToggleOn: true
        };
    }


    handleClick = () => {
        ReactDOM.render(<SearchView/>, document.getElementById('root'))
        localStorage.clear();
    }


    handleToggle = () => {
        this.setState(prevState => ({
            isToggleOn: !prevState.isToggleOn
        }));
        this.handleConversion();
    }


    handleConversion = () => {
        if (this.state.isToggleOn == true) {
            const measure = "imperial";
            localStorage.setItem("measureKey", measure);
            this.componentDidMount();
        }
        if (this.state.isToggleOn == false) {
            const measure = "metric";
            localStorage.setItem("measureKey", measure);
            this.componentDidMount();
        }
    }


    componentDidMount() {
        this.setState({isLoading: true});
        const city = localStorage.getItem("cityKey");
        const latitude = localStorage.getItem("latKey");
        const longitude = localStorage.getItem("lonKey");
        const appId = "APPID=93d0173a9739aee81321dcd177e46420";
        const measure = localStorage.getItem("measureKey");

        function getRequest() {
            if (city != null) {
                return 'https://api.openweathermap.org/data/2.5/forecast?q=' + city + '&units=' + measure + '&' + appId;
            } else return 'https://api.openweathermap.org/data/2.5/forecast?lat=' + latitude + '&lon=' + longitude + '&units=' + measure + '&' + appId;
        }

        fetch(getRequest())
            .then(response => {
                if (response.status === 200) {
                    return response.json();
                }
                if (response.status == 404) {
                    alert('City not found!')
                } else {
                    throw new Error('Something went wrong ...');
                }
            })
            .then(data => this.setState({list: data.list, isLoading: false}))
            .catch(error => this.setState({error, isLoading: false}));
    }


    render() {

        const {list} = this.state;
        const city = localStorage.getItem("cityKey");

        const moment = require('moment');
        const todaysDateTime = moment().format();
        const todaysDateTimeArray = todaysDateTime.split('T');
        const today = (todaysDateTimeArray[0]);

        const longTermForecastList = list.filter(listItem => listItem.dt_txt.includes("12:00") && !listItem.dt_txt.includes(today));
        const todaysForecastList = list.filter(listItem => listItem.dt_txt.includes(today));
        const todaysForecastTimes = todaysForecastList.map(listItem => listItem.dt_txt);

        const todaysForecastTimesString = todaysForecastTimes.toString();
        const todaysForecastNext = todaysForecastTimesString.split(',');
        const nextForecast = (todaysForecastNext[0]);
        const todaysCurrentWeather = todaysForecastList.filter(listItem => listItem.dt_txt.includes(nextForecast));

        const morning = todaysForecastList.filter(listItem => listItem.dt_txt.includes("09:00"));
        const day = todaysForecastList.filter(listItem => listItem.dt_txt.includes("15:00"));
        const evening = todaysForecastList.filter(listItem => listItem.dt_txt.includes("21:00"));
        const night = todaysForecastList.filter(listItem => listItem.dt_txt.includes("03:00"));

        //Source: https://stackoverflow.com/questions/48387180/is-it-possible-to-capitalize-first-letter-of-text-string-in-react-native-how-to
        const CapitalizedText = (props) => {
            let text = props.children.slice(0, 1).toUpperCase() + props.children.slice(1, props.children.length);
            return text;
        }

        const getTempIcon = () => {
            const measureValue = localStorage.getItem("measureKey");
            if (measureValue === 'metric') {
                return '°C';
            }
            if (measureValue === 'imperial') {
                return '°F';
            }
        }

        const tempIcon = getTempIcon();

        const dict = {
            '01d': <i className='wi wi-day-sunny'></i>,
            '02d': <i className='wi wi-day-cloudy'></i>,
            '03d': <i className='wi wi-cloud'></i>,
            '04d': <i className='wi wi-night-sleet'></i>,
            '09d': <i className='wi wi-showers'></i>,
            '10d': <i className='wi wi-night-sleet'></i>,
            '11d': <i className='wi wi-thunderstorm'></i>,
            '13d': <i className='wi wi-snow'></i>,
            '50d': <i className='wi wi-fog'></i>,
            '01n': <i className='wi wi-night-clear'></i>,
            '02n': <i className='wi wi-night-alt-cloudy'></i>,
            '03n': <i className='wi wi-night-alt-cloudy-high'></i>,
            '04n': <i className='wi wi-cloudy'></i>,
            '09n': <i className='wi wi-night-alt-sprinkle'></i>,
            '10n': <i className='wi wi-night-alt-showers'></i>,
            '11n': <i className='wi wi-night-alt-thunderstorm'></i>,
            '13n': <i className='wi wi-night-alt-snow'></i>,
            '50n': <i className='wi wi-night-fog'></i>,
        };

        return (
            <div className="result-grid-container">
                <div id="result-middle-div">
                    <div id="result-buttons-adjacent">
                        <div className="result-grid-item result-item1">
                            <p id="aligned-items"><ArrowBack input type="text" value={city} onClick={this.handleClick}/>
                                <b style={{fontSize: 30}}>{city}</b></p>
                        </div>
                        <div className="result-grid-item result-item2">
                            <p><Switch id="toggle" onClick={this.handleToggle}/></p>
                        </div>
                    </div>

                    <div className="result-grid-item result-item3">
                        <p style={{fontSize: 25}}>{moment().format('dddd, MMMM Do YYYY')}</p>
                        {todaysCurrentWeather.map(listItem =>
                            <p>
                                {listItem.weather.map(wI =>
                                    <p style={{fontSize: 22}}><CapitalizedText>{wI.description}</CapitalizedText>
                                    </p>)}
                            </p>
                        )}
                    </div>

                    <div id="today-elements-adjacent">
                        <div className="result-grid-item result-item4">
                            {todaysCurrentWeather.map(listItem =>
                                <p>
                                    <p id="today-elements">{Math.round(listItem.main.temp) + tempIcon}</p>
                                </p>
                            )}
                        </div>

                        <div className="result-grid-item result-item5">
                            {todaysCurrentWeather.map(listItem =>
                                <ul>
                                    {listItem.weather.map(wI =>
                                        <p id="today-elements">{dict[wI.icon]}</p>
                                    )}
                                </ul>
                            )}
                        </div>


                        <div className="result-grid-item result-item6">
                            <ul>
                                {morning.map(listItem =>
                                    <p>Morning {Math.round(listItem.main.temp) + tempIcon}</p>)}
                                {day.map(listItem =>
                                    <p>Day {Math.round(listItem.main.temp) + tempIcon}</p>)}
                                {evening.map(listItem =>
                                    <p>Evening {Math.round(listItem.main.temp) + tempIcon}</p>)}
                                {night.map(listItem =>
                                    <p>Night {Math.round(listItem.main.temp) + tempIcon}</p>)}
                            </ul>
                        </div>
                    </div>

                    <div className="result-grid-item result-item7" style={{fontSize: 14}}>
                        {longTermForecastList.map(listItem =>
                            <li class="longtermListItem" key={listItem.dt_txt}>
                                {moment(listItem.dt_txt).format('dddd')}
                                {listItem.weather.map(wI =>
                                    <p id="long-term-icons">{dict[wI.icon]}</p>
                                )}
                                {Math.round(listItem.main.temp) + tempIcon}
                            </li>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

export default ResultView;