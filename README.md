This is a simple weather application which uses [https://openweathermap.org/api](https://openweathermap.org/api). The user can access weather forecast for today and the next five days. Search can be done by using either geolocation or searching based on a city.

The project is developed based on tasks in Trello board [https://trello.com/b/BCRO3Ory/weather-application](https://trello.com/b/BCRO3Ory/weather-application).

Open [http://localhost:3000](http://localhost:3000) to view the project in the browser.

